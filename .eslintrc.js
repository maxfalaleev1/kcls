// eslint-disable-next-line no-undef
module.exports = {
    root: true,
    parser: '@typescript-eslint/parser',
    plugins: [
        '@typescript-eslint',
        "react",
        "react-hooks",
    ],
    extends: [
        'eslint:recommended',
        'plugin:@typescript-eslint/recommended',
        "plugin:react/recommended",
        "plugin:react-hooks/recommended"
    ],
    rules: {
        "object-curly-spacing": ["error", "always"],
        "curly": "error",
        "comma-dangle": ["error", "always-multiline"],
        "semi": [2, "always"],
        "react/prop-types": [0],
        "react/display-name": [0]
    },
    settings: {
        react: {
            version: "17.0.39"
        },
    }
};
