all: run

.PHONY: install
install:
	composer install --prefer-dist
	yarn

.PHONY: build
build:
	yarn build

.PHONY: run
run:
	php -S 0.0.0.0:8000 -t ./public

.PHONY: test
test:
	./vendor/bin/phpunit -c ./phpunit.xml.dist