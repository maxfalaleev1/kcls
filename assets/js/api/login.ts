import {typedFetch} from "./utils";

const loginEndpoint = '/auth';

export type LoginResponse = {
    errors: { [key: string]: string[] }
    token?: string
}

export const login = async (username: string, password: string): Promise<LoginResponse> => {
    return await typedFetch<LoginResponse>(loginEndpoint, {
        method: 'POST',
        credentials: 'same-origin',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({ username, password })
    });
}