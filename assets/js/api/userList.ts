import {User} from "../types";
import {typedFetch} from "./utils";

const studentEndpoint = '/users';

export type UserListResponse = {
    users: User[]
}

export const userList = async (cursor: number = 0): Promise<UserListResponse> => {
    return await typedFetch<UserListResponse>(`${studentEndpoint}?cursor=${cursor || 0}`, {
        headers: {
            'Content-Type': 'application/json'
        },
    });
}