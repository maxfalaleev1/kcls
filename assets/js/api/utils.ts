export async function typedFetch<T>(input: RequestInfo, init?: RequestInit): Promise<T> {
    const response = await fetch(input, init);
    if (!response.ok) {
        throw new Error(response.statusText)
    }
    return await response.json() as T;
}