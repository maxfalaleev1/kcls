import React, {StrictMode} from 'react';
import {render} from 'react-dom';
import {App} from "./component";

const rootNode = document.getElementById('root');
const useStrict = false;
if (rootNode) {
    render(useStrict ? (
        <StrictMode>
            <App/>
        </StrictMode>
    ) : (
        <App/>
    ), rootNode);
}

if (module.hot) {
    module.hot.accept();
}