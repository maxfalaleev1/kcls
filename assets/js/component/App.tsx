import React, {FC, useCallback, useState} from "react";
import {LoginForm} from "./LoginForm";
import {isAuthenticated, persistToken} from "../utils";
import {UserList} from "./UserList";

export const App: FC = () => {
    const [hasUser, setHasUser] = useState<string | null>(isAuthenticated());

    const onSuccess = useCallback((token: string, remember: boolean) => {
        persistToken(token, remember);
        setHasUser(token);
    }, []);

    return hasUser ? (
        <UserList/>
    ) : (
        <LoginForm onSuccess={onSuccess}/>
    );
}