import React, {FC, useCallback, useState} from "react";
import {login} from "../api";

export type LoginFormProps = {
    onSuccess(token: string, remember: boolean): void;
}

export type LoginFormInput = {
    username: string
    password: string
    remember: boolean
}

export type FormErrors = {
    [key: string]: string[]
}

export const LoginForm: FC<LoginFormProps> = ({onSuccess}) => {
    const [values, setValues] = useState<LoginFormInput>({
        username: '',
        password: '',
        remember: false,
    });
    const [errors, setErrors] = useState<FormErrors>({});
    const [error, setError] = useState<Error | null>(null);

    const onSubmit = useCallback(async (e) => {
        e.preventDefault();
        console.log(values);

        try {
            const data = await login(values.username, values.password);

            const errors = data.errors || {};
            setErrors(errors);

            if (Object.keys(errors).length === 0 && data.token) {
                onSuccess(data.token, Boolean(values.remember));
            }
        } catch (e) {
            console.log(e);
            setError(e as Error);
        }

    }, [values]);

    return (
        <form
            onSubmit={onSubmit}
            className="b-login__form">
            <div className="b-logo"/>

            <div className="b-login__header">
                <div className="b-login__heading">Welcome to the Learning Management System</div>
                <div className="b-login__text">Please log in to continue</div>
            </div>

            {error && (
                <div className="b-login__row">
                    <div className="b-error">{error.message}</div>
                </div>
            )}

            <div className="b-login__row">
                <input
                    type="text"
                    name="username"
                    placeholder="Some text"
                    className="b-input b-input--icon b-input--username"
                    value={values.username}
                    onChange={e => setValues({...values, username: e.target.value})}
                />
                {errors.username && (
                    <div className="b-errors">
                        {errors.username.map(err => (
                            <div className='b-error'>{err}</div>
                        ))}
                    </div>
                )}
            </div>
            <div className="b-login__row">
                <input
                    type="password"
                    name="password"
                    placeholder="Some text"
                    className="b-input b-input--icon b-input--password"
                    value={values.password}
                    onChange={e => setValues({...values, password: e.target.value})}
                />
                {errors.password && (
                    <div className="b-errors">
                        {errors.password.map(err => (
                            <div className='b-error'>{err}</div>
                        ))}
                    </div>
                )}
            </div>
            <div className="b-login__row">
                <input
                    type="checkbox"
                    id="remember"
                    name="remember"
                    placeholder="Some text"
                    checked={values.remember}
                    value="1"
                    onChange={e => setValues({...values, remember: e.target.checked})}
                />
                <label htmlFor="remember">Remember me</label>
            </div>
            <div className="b-login__row">
                <input
                    type="submit"
                    className="b-button b-button--signin"
                    value="Log In"
                />
            </div>
        </form>
    );
}