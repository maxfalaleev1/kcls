import React, {FC, useCallback, useEffect, useState} from "react";
import qs from 'query-string';
import {removeToken} from "../utils";
import {LogOut} from "react-feather";
import {UserListItem} from "./UserListItem";
import {User} from "../types";
import {userList} from "../api";

export const UserList: FC = () => {
    const [loading, setLoading] = useState(false);
    const [users, setUsers] = useState<User[]>([]);
    const params: { cursor?: string } = qs.parse(window.location.search);
    const [cursor, setCursor] = useState<number>(params.cursor ? parseInt(params.cursor, 10) : 0);

    useEffect(() => {
        setCursor(
            params.cursor
                ? parseInt(params.cursor, 10)
                : 0
        );
    }, [params.cursor]);

    useEffect(() => {
        setLoading(true);
        userList(cursor || 0).then(data => {
            setLoading(false);
            setUsers(data.users);
        }).catch(() => {
            setLoading(false);
        });
    }, [cursor]);

    const onLogout = useCallback((e) => {
        e.preventDefault();
        if (window.confirm('you really want to logout?')) {
            removeToken();
            // ugly hack for refresh current page
            window.location.pathname = window.location.pathname;
        }
    }, []);

    const onLoadMore = useCallback((e) => {
        e.preventDefault();
        setCursor(cursor + users[users.length - 1].id);
    }, [users]);

    return (
        <>
            <h1 className="b-user-list__heading">User List</h1>
            {loading ? (
                <div>Loading...</div>
            ) : (
                <div>
                    <div className="b-user-list__header"/>
                    {users.length > 0 ? users.map((user, i) => (
                        <>
                            <UserListItem key={i} even={i % 2 === 0} user={user}/>
                            <a href='#' onClick={onLoadMore} className="b-loadmore">
                                Load more (Cursor: {cursor} by 10 entries)
                            </a>
                        </>
                    )) : (
                        <div>Users not found</div>
                    )}
                </div>
            )}
            <a href='#' onClick={onLogout} className="b-logout-link">
                <LogOut className="b-logout-link__icon"/> Log out
            </a>
        </>
    )
}