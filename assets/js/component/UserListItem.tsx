import React, {FC} from "react";
import {User} from "../types";
import {bem} from "classnames-bem";
import {CheckCircle, MoreHorizontal} from "react-feather";

export type UserListItemProps = {
    user: User
    even?: boolean
}

export const UserListItem: FC<UserListItemProps> = ({ user, even }) => {
    return (
        <div className={bem('b-user-list__row', { even })}>
            <div className='b-user-list__icon-col'>
                <CheckCircle className='b-user-list__check' />
            </div>
            <div>
                <div className="b-user-list__username">{user.username}</div>
                <div className="b-user-list__fullname">Firstname Lastname</div>
            </div>
            <div>
                <MoreHorizontal/>
                <div>Default group</div>
            </div>
        </div>
    );
}