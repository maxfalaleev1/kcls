const tokenKey = "insecure_auth_token";

let temporaryToken: string | null = null;

export const persistToken = (token: string, remember: boolean): void => {
    temporaryToken = token;
    if (remember) {
        window.sessionStorage.setItem(tokenKey, token);
    }
}

export const removeToken = (): void => {
    temporaryToken = null;
    window.sessionStorage.removeItem(tokenKey);
}

export const isAuthenticated = (): string | null => {
    if (temporaryToken) {
        return temporaryToken;
    }
    return window.sessionStorage.getItem(tokenKey);
}