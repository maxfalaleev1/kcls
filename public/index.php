<?php

declare(strict_types=1);

use Max\Kcls\Controller\LoginController;
use Max\Kcls\Controller\HomepageController;
use Max\Kcls\Controller\LogoutController;
use Max\Kcls\Controller\UserListController;
use Max\Kcls\Database\Database;
use Max\Kcls\Exceptions\NotFoundException;
use Max\Kcls\InternalResponseFactory\InternalResponseFactory;
use Max\Kcls\InternalResponseFactory\InternalResponseFactoryInterface;
use Max\Kcls\Kernel;
use Max\Kcls\Repository\UserRepository;
use Max\Kcls\Router\Router;
use Max\Kcls\Service\Password;
use Max\Kcls\ServiceLocator\Definition;
use Max\Kcls\ServiceLocator\ServiceLocator;
use Max\Kcls\Template\Template;
use Max\Kcls\Template\TemplateInterface;
use Nyholm\Psr7\Factory\Psr17Factory;
use Nyholm\Psr7Server\ServerRequestCreator;
use Max\Kcls\ServiceLocator\Reference;

include(__DIR__ . '/../vendor/autoload.php');

$databaseUri = getenv('DATABASE_URL');
if (empty($databaseUri)) {
    $databaseUri = 'mysql://root:123456@127.0.0.1:3306/app';
}

$locator = new ServiceLocator([
    // controllers
    HomepageController::class => new Definition(HomepageController::class),
    LoginController::class => new Definition(LoginController::class),
    LogoutController::class => new Definition(LogoutController::class),
    UserListController::class => new Definition(UserListController::class),
    // database
    Database::class => new Definition(Database::class, [
        'uri' => $databaseUri
    ]),
    UserRepository::class => new Definition(UserRepository::class, [
        'database' => new Reference(Database::class)
    ]),
    // services
    Password::class => new Definition(Password::class),
    InternalResponseFactoryInterface::class => new Definition(InternalResponseFactory::class, [
        'template' => new Reference(TemplateInterface::class)
    ]),
    TemplateInterface::class => new Definition(Template::class, [
        'root' => __DIR__ . '/../templates',
        'globals' => [
            'copyright_year' => date('Y')
        ]
    ]),
]);

//if (getenv('CREATE_SCHEMA')) {
///** @var Database $database */
//$database = $locator->get(Database::class);
//$database->createSchema();
//$database->loadFixtures();
//}

$router = new Router([
    '/' => HomepageController::class,
    '/auth' => LoginController::class,
    '/logout' => LogoutController::class,
    '/users' => UserListController::class,
]);

$kernel = new Kernel(
    $router,
    $locator,
);

$psr17Factory = new Psr17Factory();
$creator = new ServerRequestCreator($psr17Factory, $psr17Factory, $psr17Factory, $psr17Factory);

$serverRequest = $creator->fromGlobals();

$kernel->run($serverRequest);