<?php

declare(strict_types=1);

namespace Max\Kcls\ArgumentResolver;

use Max\Kcls\ServiceLocator\ServiceLocatorInterface;
use ReflectionMethod;
use ReflectionNamedType;

class ArgumentResolver
{
    public function __construct(
        protected readonly ServiceLocatorInterface $serviceLocator
    ) {}

    public function invoke($instance): mixed
    {
        $fn = new \ReflectionMethod($instance, '__invoke');

        $invokeArgs = [];
        foreach ($this->getArguments($fn) as $key => $value) {
            $invokeArgs[$key] = $this->serviceLocator->get($value);
        }
        return $fn->invokeArgs($instance, $invokeArgs);
    }

    public function getArguments(ReflectionMethod $reflectionMethod): array
    {
        $attrs = [];
        $parameters = $reflectionMethod->getParameters();
        foreach ($parameters as $arg) {
            /** @var ReflectionNamedType $type */
            $type = $arg->getType();
            if ($type->isBuiltin()) {
                continue;
            }

            $attrs[$arg->getName()] = $type->getName();
        }

        return $attrs;
    }
}