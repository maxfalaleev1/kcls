<?php

declare(strict_types=1);

namespace Max\Kcls\Controller;

use Max\Kcls\InternalResponseFactory\InternalResponseFactoryInterface;
use Max\Kcls\ServiceLocator\ServiceLocatorAwareInterface;
use Max\Kcls\ServiceLocator\ServiceLocatorTrait;
use Psr\Http\Message\ServerRequestInterface;

class HomepageController implements ServiceLocatorAwareInterface
{
    use ServiceLocatorTrait;

    public function __invoke(
        ServerRequestInterface           $request,
        InternalResponseFactoryInterface $responseFactory,
    )
    {
        return $responseFactory->template(200, 'homepage.php');
    }
}