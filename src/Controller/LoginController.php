<?php

declare(strict_types=1);

namespace Max\Kcls\Controller;

use Max\Kcls\Exceptions\AccessDeniedException;
use Max\Kcls\InternalResponseFactory\InternalResponseFactoryInterface;
use Max\Kcls\Repository\UserRepository;
use Max\Kcls\Service\Password;
use Max\Kcls\ServiceLocator\ServiceLocatorAwareInterface;
use Max\Kcls\ServiceLocator\ServiceLocatorTrait;
use Psr\Http\Message\ServerRequestInterface;

class LoginController implements ServiceLocatorAwareInterface
{
    use ServiceLocatorTrait;

    public function __invoke(
        ServerRequestInterface           $request,
        InternalResponseFactoryInterface $responseFactory,
        Password $password,
        UserRepository $userRepository,
    )
    {
        if ($request->getMethod() === 'DELETE') {
            return $responseFactory->template(200, 'logout.php');
        }

        if ($request->getMethod() !== 'POST') {
            throw new AccessDeniedException();
        }

        // @todo convert arr into dto
        // @todo dto validation
        $params = json_decode($request->getBody()->getContents(), true);

        $errors = [];
        if (empty($params['username'])) {
            $errors['username'] = ['should not be empty'];
        }
        if (empty($params['password'])) {
            $errors['password'] = ['should not be empty'];
        }

        if (count($errors) > 0) {
            return $responseFactory->json(400, [
                // Never say "user not found" due to security reasons.
                'errors' => $errors
            ]);
        }

        $user = $userRepository->findByUsername($params['username']);

        $defaultError = 'username or password invalid';
        if (null === $user) {
            $errors['username'] = [$defaultError];
            return $responseFactory->json(400, [
                // Never say "user not found" due to security reasons.
                'errors' => $errors
            ]);
        }

        if (!$password->verify($params['password'], $user->password)) {
            $errors['password'] = [$defaultError];
            return $responseFactory->json(400, [
                // Never say "user not found" due to security reasons.
                'errors' => $errors
            ]);
        }

        return $responseFactory->json(200, [
            'token' => 'insecure_example_token'
        ]);
    }
}