<?php

declare(strict_types=1);

namespace Max\Kcls\Controller;

use Max\Kcls\InternalResponseFactory\InternalResponseFactoryInterface;
use Psr\Http\Message\ServerRequestInterface;

class LogoutController
{
    public function __invoke(
        ServerRequestInterface           $request,
        InternalResponseFactoryInterface $responseFactory,
    )
    {
        return $responseFactory->template(200, 'logout.php');
    }
}