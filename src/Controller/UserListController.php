<?php

declare(strict_types=1);

namespace Max\Kcls\Controller;

use Max\Kcls\InternalResponseFactory\InternalResponseFactoryInterface;
use Max\Kcls\Repository\UserRepository;
use Max\Kcls\Service\Password;
use Max\Kcls\ServiceLocator\ServiceLocatorAwareInterface;
use Max\Kcls\ServiceLocator\ServiceLocatorTrait;
use Psr\Http\Message\ServerRequestInterface;

class UserListController implements ServiceLocatorAwareInterface
{
    use ServiceLocatorTrait;

    public function __invoke(
        ServerRequestInterface           $request,
        InternalResponseFactoryInterface $responseFactory,
        Password $password,
        UserRepository $userRepository,
    )
    {
        $query = $request->getQueryParams();

        $cursor = $query['cursor'] ?? 0;
        if ($cursor < 0) {
            $cursor = 0;
        }

        return $responseFactory->json(200, [
            'users' => $userRepository->findList((int)$cursor)
        ]);
    }
}