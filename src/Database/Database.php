<?php

declare(strict_types=1);

namespace Max\Kcls\Database;

use PDO;

class Database
{
    protected PDO $pdo;

    public function __construct(string $uri)
    {
        $params = parse_url($uri);
        $this->pdo = new PDO(sprintf(
            'mysql:host=%s;dbname=%s',
            $params['host'],
            trim($params['path'], '/')
        ), $params['user'], $params['pass'], [
            PDO::ATTR_EMULATE_PREPARES,
            PDO::ATTR_ERRMODE,
            PDO::ERRMODE_WARNING
        ]);
    }

    public function getPDO(): PDO
    {
        return $this->pdo;
    }

    /**
     * @internal
     */
    public function createSchema(): void
    {
        $sqls = [
            "drop table if exists api_users",
            "create table if not exists api_users
(
    id       bigint auto_increment
        primary key,
    username varchar(255) not null,
    password varchar(255) not null,
    constraint api_users_username_uindex
        unique (username),
    constraint api_users_username_uindex_2
        unique (username)
);",
            "drop table if exists students",
            "create table if not exists students
(
    id      bigint auto_increment
        primary key,
    user_id int          not null,
    name    varchar(255) not null
);"
        ];
        foreach ($sqls as $sql) {
            $this->pdo->query($sql)->execute();
        }
    }

    /**
     * @internal
     */
    public function loadFixtures(): void
    {
        $fixtures = [
            'demo' => ['example1', 'example2']
        ];

        foreach ($fixtures as $user => $students) {
            $password = password_hash($user, PASSWORD_BCRYPT, [
                'cost' => 14
            ]);

            $this->pdo
                ->prepare("INSERT INTO api_users (id, username, password) VALUES (DEFAULT, ?, ?);")
                ->execute([$user, $password]);

            $lastInsertId = $this->pdo->lastInsertId();

            foreach ($students as $student) {
                $this->pdo
                    ->prepare("INSERT INTO students (id, user_id, name) VALUES (DEFAULT, ?, ?);")
                    ->execute([$lastInsertId, $student]);
            }
        }
    }
}