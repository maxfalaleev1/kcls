<?php

declare(strict_types=1);

namespace Max\Kcls\Entity;

class User
{
    public string $username;
    public string $password;
}