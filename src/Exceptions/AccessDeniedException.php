<?php

declare(strict_types=1);

namespace Max\Kcls\Exceptions;

class AccessDeniedException extends \Exception
{
    public function __construct()
    {
        parent::__construct('access denied');
    }
}