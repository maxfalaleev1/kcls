<?php

declare(strict_types=1);

namespace Max\Kcls\Exceptions;

class ContainerAlreadyCompiled extends \Exception
{
    public function __construct()
    {
        parent::__construct('container already compiled');
    }
}
