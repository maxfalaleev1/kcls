<?php

declare(strict_types=1);

namespace Max\Kcls\Exceptions;

class NotFoundException extends \Exception
{
    public function __construct()
    {
        parent::__construct('page not found');
    }
}