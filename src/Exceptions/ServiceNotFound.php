<?php

declare(strict_types=1);

namespace Max\Kcls\Exceptions;

class ServiceNotFound extends \Exception
{
    public function __construct(string $name)
    {
        parent::__construct(sprintf(
            'service %s not found',
            $name
        ));
    }
}