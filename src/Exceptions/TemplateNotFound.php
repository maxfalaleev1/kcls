<?php

declare(strict_types=1);

namespace Max\Kcls\Exceptions;

class TemplateNotFound extends \Exception
{
    public function __construct(string $root, string $template)
    {
        parent::__construct(sprintf(
            'Template %s not found in %s',
            $root,
            $template
        ));
    }
}