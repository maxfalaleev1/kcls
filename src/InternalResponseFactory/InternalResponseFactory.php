<?php

declare(strict_types=1);

namespace Max\Kcls\InternalResponseFactory;

use Lmc\HttpConstants\Header;
use Max\Kcls\Template\TemplateInterface;
use Nyholm\Psr7\Response;
use Psr\Http\Message\ResponseInterface;

class InternalResponseFactory implements InternalResponseFactoryInterface
{
    public function __construct(
        protected readonly TemplateInterface $template
    )
    {
    }

    public function template(int $statusCode, string $template, array $data = []): ResponseInterface
    {
        return new Response(
            $statusCode,
            [Header::CONTENT_TYPE => 'text/html'],
            $this->template->render($template, $data),
        );
    }

    public function json(int $statusCode, array $content): ResponseInterface
    {
        return new Response(
            $statusCode,
            [Header::CONTENT_TYPE => 'application/json'],
            json_encode($content),
        );
    }
}