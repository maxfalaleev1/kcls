<?php

declare(strict_types=1);

namespace Max\Kcls\InternalResponseFactory;

use Psr\Http\Message\ResponseInterface;

interface InternalResponseFactoryInterface
{
    public function template(int $statusCode, string $template, array $data = []): ResponseInterface;

    public function json(int $statusCode, array $content): ResponseInterface;
}