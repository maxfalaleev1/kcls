<?php

declare(strict_types=1);

namespace Max\Kcls;

use Max\Kcls\ArgumentResolver\ArgumentResolver;
use Max\Kcls\Exceptions\NotFoundException;
use Max\Kcls\Router\RouterInterface;
use Max\Kcls\ServiceLocator\ServiceLocatorInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

final class Kernel
{
    private ArgumentResolver $argumentResolver;

    public function __construct(
        protected readonly RouterInterface $router,
        protected readonly ServiceLocatorInterface $serviceLocator,
    )
    {
        $this->argumentResolver = new ArgumentResolver($this->serviceLocator);
    }

    public function run(ServerRequestInterface $request): void
    {
        $this->serviceLocator->define(ServerRequestInterface::class, $request);
        $this->serviceLocator->build();

        $uriSegment = $this->router->match($request->getRequestTarget());
        if (null === $uriSegment) {
            throw new NotFoundException();
        }

        $instance = $this->serviceLocator->get($uriSegment);
        $response = $this->argumentResolver->invoke($instance);
        if (false === ($response instanceof ResponseInterface)) {
            throw new \RuntimeException('controller return type response should be Response');
        }

        $this->send($response);
    }

    protected function send(ResponseInterface $response): void
    {
        foreach ($response->getHeaders() as $key => $headers) {
            foreach ($headers as $headerLine) {
                header(sprintf('%s: %s', $key, $headerLine));
            }
        }
        echo $response->getBody();
    }
}