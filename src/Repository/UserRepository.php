<?php

declare(strict_types=1);

namespace Max\Kcls\Repository;

use Max\Kcls\Database\Database;
use Max\Kcls\Entity\User;
use PDO;

class UserRepository
{
    public function __construct(
        protected readonly Database $database
    )
    {
    }

    public function findByUsername(string $username): ?User
    {
        $sql = <<<SQL
select * 
from api_users
where username = :username
SQL;

        $pdo = $this->database->getPDO();
        $stmt = $pdo->prepare($sql);
        $stmt->execute([
            ':username' => $username,
        ]);
        // @todo check errCode
        $result = $stmt->fetchObject(User::class);
        if ($result instanceof User) {
            return $result;
        }

        return null;
    }

    /**
     * @param int $id
     * @return array<int, User>|User[]
     */
    public function findList(int $id): array
    {
        $sql = <<<SQL
select id, username
from api_users
where id > :id
limit 10
SQL;

        $pdo = $this->database->getPDO();
        $stmt = $pdo->prepare($sql);
        $stmt->execute([':id' => $id]);

        return $stmt->fetchAll(PDO::FETCH_CLASS, User::class);
    }
}