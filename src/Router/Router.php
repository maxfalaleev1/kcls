<?php

declare(strict_types=1);

namespace Max\Kcls\Router;

class Router implements RouterInterface
{
    public function __construct(
        protected array $routes = []
    ) {
    }

    public function match(string $requestUri): ?string
    {
        $segment = strtok($requestUri, '?');

        return $this->routes[$segment] ?? null;
    }
}