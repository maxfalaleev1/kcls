<?php

declare(strict_types=1);

namespace Max\Kcls\Router;

interface RouterInterface
{
    public function match(string $requestUri): ?string;
}