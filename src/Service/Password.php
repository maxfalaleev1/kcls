<?php

declare(strict_types=1);

namespace Max\Kcls\Service;

class Password
{
    public function create(string $password): string
    {
        return password_hash($password, PASSWORD_BCRYPT, [
            'cost' => 14
        ]);
    }

    public function verify(string $password, string $hash): bool
    {
        return password_verify($password, $hash);
    }
}