<?php

declare(strict_types=1);

namespace Max\Kcls\ServiceLocator;

class Definition
{
    public function __construct(
        public readonly string $name,
        public readonly array $args = []
    )
    {
    }
}