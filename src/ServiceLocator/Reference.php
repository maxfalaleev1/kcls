<?php

declare(strict_types=1);

namespace Max\Kcls\ServiceLocator;

class Reference
{
    public function __construct(
        public readonly string $name
    )
    {
    }
}