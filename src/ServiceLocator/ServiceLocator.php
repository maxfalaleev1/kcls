<?php

declare(strict_types=1);

namespace Max\Kcls\ServiceLocator;

use Max\Kcls\Exceptions\ContainerAlreadyCompiled;
use Max\Kcls\Exceptions\ServiceAlreadyDefined;
use Max\Kcls\Exceptions\ServiceNotFound;

class ServiceLocator implements ServiceLocatorInterface
{
    protected $instances = [];

    private bool $completed = false;

    /**
     * @param array<string, Definition|Reference> $services
     */
    public function __construct(
        protected array $services = []
    )
    {
    }

    public function build(): void
    {
        $this->completed = true;
    }

    /**
     * @inheritDoc
     */
    public function define(string $key, $definition): void
    {
        if ($this->completed) {
            throw new ContainerAlreadyCompiled();
        }

        if (array_key_exists($key, $this->services)) {
            throw new ServiceAlreadyDefined($key);
        }

        if ($definition instanceof Definition) {
            $this->services[$key] = $definition;
        } else {
            $this->instances[$key] = $definition;
            $this->services[$key] = new Definition(get_class($definition));
        }
    }

    public function get(string $key): mixed
    {
        if (!array_key_exists($key, $this->services)) {
            throw new ServiceNotFound($key);
        }

        $this->services[$key] = $this->instantiate($key, $this->services[$key]);

        return $this->services[$key];
    }

    protected function instantiate(string $key, Definition|Reference $item): mixed
    {
        if (array_key_exists($key, $this->instances)) {
            return $this->instances[$key];
        }

        switch (get_class($item)) {
            case Definition::class:
                $reflect = new \ReflectionClass($item->name);

                $args = [];
                foreach ($item->args as $name => $arg) {
                    if ($arg instanceof Reference) {
                        $args[$name] = $this->instantiate($arg->name, $this->services[$arg->name]);
                    } else {
                        $args[$name] = $arg;
                    }
                }

                return $reflect->newInstanceArgs($args);

            case Reference::class:
                return $this->instantiate($key, $item->name);
        }

        throw new \RuntimeException('unreachable state');
    }
}