<?php

declare(strict_types=1);

namespace Max\Kcls\ServiceLocator;

interface ServiceLocatorAwareInterface
{
    public function getServiceLocator(): ServiceLocatorInterface;
}