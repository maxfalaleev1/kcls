<?php

declare(strict_types=1);

namespace Max\Kcls\ServiceLocator;

interface ServiceLocatorInterface
{
    public function get(string $key): mixed;

    /**
     * @internal
     * @param string $key
     * @param $definition
     * @return void
     */
    public function define(string $key, $definition): void;

    public function build(): void;
}