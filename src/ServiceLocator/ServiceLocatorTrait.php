<?php

declare(strict_types=1);

namespace Max\Kcls\ServiceLocator;

use RuntimeException;

trait ServiceLocatorTrait
{
    protected ?ServiceLocatorInterface $serviceLocator;

    public function setServiceLocator(ServiceLocatorInterface $serviceLocator): void
    {
        if ($this->serviceLocator) {
            throw new RuntimeException('service locator already initialized');
        }

        $this->serviceLocator = $serviceLocator;
    }

    public function getServiceLocator(): ServiceLocatorInterface
    {
        if (null === $this->serviceLocator) {
            throw new RuntimeException('service locator is null');
        }

        return $this->serviceLocator;
    }
}