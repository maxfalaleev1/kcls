<?php

declare(strict_types=1);

namespace Max\Kcls\Template;

use Max\Kcls\Exceptions\TemplateNotFound;

class Template implements TemplateInterface
{
    public function __construct(
        protected readonly string $root,
        protected readonly array $globals = []
    )
    {
    }

    protected function getTemplate(string $template): string
    {
        $path = realpath(rtrim($this->root, '/') . '/' . ltrim($template, '/'));
        if (!$path) {
            throw new TemplateNotFound($this->root, $template);
        }

        return $path;
    }

    public function render(string $template, ?array $data = []): string
    {
        $tpl = $this->getTemplate($template);

        extract(array_merge($this->globals, $data, [
            'template' => $this
        ]));
        ob_start();
        include($tpl);
        return ob_get_clean();
    }
}