<?php

declare(strict_types=1);

namespace Max\Kcls\Template;

interface TemplateInterface
{
    public function render(string $template, ?array $data = []): string;
}