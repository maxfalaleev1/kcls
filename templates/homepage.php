<?php
/** @var TemplateInterface $template */
/** @var string $copyright_year */

use Max\Kcls\Template\TemplateInterface;
?>
<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <meta name="theme-color" content="#000000"/>
    <meta name="description" content="just a simple test"/>
    <title>hello world</title>
    <link rel="stylesheet" href="/build/css/app.css"/>
</head>
<body class="b-body">
<main class="b-main">
    <div id="root"></div>
    <?= $template->render('/block/footer.php') ?>
</main>
<script type="application/javascript" src="/build/js/app.js"></script>
</body>
</html>
