<?php
/** @var TemplateInterface $template */
/** @var string $copyright_year */

use Max\Kcls\Template\TemplateInterface;
?>
<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <meta name="theme-color" content="#000000"/>
    <meta name="description" content="just a simple test"/>
    <title>hello world</title>
    <link rel="stylesheet" href="/build/css/app.css"/>
</head>
<body class="b-body">
<main class="b-main">
    If application is stateless we whould use session storage or http-only cookie
    with any token (jwt as example).
    So... if application is stateful we remove cookie and drop session data and then redirect
    to login page as example.
    In this case we use react and we can do not store key in backend.
</main>
<script type="application/javascript" src="/build/js/app.js"></script>
</body>
</html>
