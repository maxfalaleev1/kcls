<?php

declare(strict_types=1);

namespace Max\Kcls\Tests\ArgumentResolver;

use Max\Kcls\ArgumentResolver\ArgumentResolver;
use Max\Kcls\ServiceLocator\Definition;
use Max\Kcls\ServiceLocator\ServiceLocator;
use PHPUnit\Framework\TestCase;

class Foobar
{
    public int $counter = 0;

    public function __invoke()
    {
        $this->counter += 1;
    }
}

class ArgumentResolverTest extends TestCase
{
    public function testParameters(): void
    {
        $serviceLocator = new ServiceLocator([
            Foobar::class => new Definition(Foobar::class)
        ]);

        $resolver = new ArgumentResolver($serviceLocator);
        /** @var Foobar $instance */
        $instance = $serviceLocator->get(Foobar::class);
        $resolver->invoke($instance);
        self::assertSame(1, $instance->counter);
    }
}