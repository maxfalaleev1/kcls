<?php

declare(strict_types=1);

namespace Max\Kcls\Tests\Repository;

use Max\Kcls\Database\Database;
use PHPUnit\Framework\TestCase;

class UserRepository extends TestCase
{
    public function testFindUser(): void
    {
        $database = new Database('mysql://root:123456@127.0.0.1:3306/app');
        $database->createSchema();
        $database->loadFixtures();

        $repo = new \Max\Kcls\Repository\UserRepository($database);
        $user = $repo->findByUsername('demo');
        self::assertNotNull($user);
    }

    public function testFindUsers(): void
    {
        $database = new Database('mysql://root:123456@127.0.0.1:3306/app');
        $database->createSchema();
        $database->loadFixtures();

        $repo = new \Max\Kcls\Repository\UserRepository($database);
        $users = $repo->findList(0);
        self::assertCount(1, $users);
    }
}