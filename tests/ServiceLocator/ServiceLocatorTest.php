<?php

declare(strict_types=1);

namespace Max\Kcls\Tests\ServiceLocator;

use Max\Kcls\ServiceLocator\Definition;
use Max\Kcls\ServiceLocator\Reference;
use Max\Kcls\ServiceLocator\ServiceLocator;
use PHPUnit\Framework\TestCase;

class ServiceLocatorTest extends TestCase
{
    public function testServiceLocator(): void
    {
        $locator = new ServiceLocator([
            'dep' => new Definition(FactoryDependency::class),
            'foobar' => new Definition(Factory::class, [
                'dependency' => new Reference('dep')
            ]),
        ]);

        /** @var Factory $instance */
        $instance = $locator->get('foobar');

        self::assertInstanceOf(Factory::class, $instance);
        self::assertInstanceOf(FactoryDependency::class, $instance->dependency);
    }
}