<?php

declare(strict_types=1);

namespace Max\Kcls\Tests;

use Max\Kcls\Exceptions\TemplateNotFound;
use Max\Kcls\Template\Template;
use PHPUnit\Framework\TestCase;

class TemplateTest extends TestCase
{
    public function testNoTemplateFound(): void
    {
        $tpl = new Template(__DIR__ . '/templates', [
            'hello' => 'world'
        ]);
        self::expectException(TemplateNotFound::class);
        $tpl->render('unknown.php');
    }

    public function testPrimitiveTemplateEngine(): void
    {
        $tpl = new Template(__DIR__ . '/templates', [
            'hello' => 'world'
        ]);
        $output = $tpl->render('hello.php', [
            'phone' => 123
        ]);
        self::assertStringContainsString('world', $output);
        self::assertStringContainsString('123', $output);
    }
}