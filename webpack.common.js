/* eslint-disable */
const webpack = require('webpack');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const path = require('path');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const Dotenv = require('dotenv-webpack');
const sass = require('sass');

const currentDirectory = process.cwd();

const copy = (src, dst) => ({
    from: path.resolve(currentDirectory, src),
    to: path.resolve(currentDirectory, dst),
});

module.exports = {
    entry: {
        app: [
            path.resolve(currentDirectory, './assets/js/app.tsx'),
            path.resolve(currentDirectory, './assets/css/app.scss')
        ],
    },
    module: {
        rules: [
            {
                test: /\.(js|jsx|ts|tsx)$/,
                exclude: /(node_modules|bower_components)/,
                use: {
                    loader: "swc-loader",
                    options: {
                        sync: true,
                        jsc: {
                            parser: {
                                jsx: true,
                                syntax: "typescript"
                            }
                        }
                    }
                }
            },
            {
                test: /\.(?:ico|gif|png|jpg|jpeg|svg)$/i,
                type: 'asset/resource',
            },
            {
                test: /\.(woff(2)?|eot|ttf|otf)$/,
                type: 'asset',
                parser: {
                    dataUrlCondition: {
                        maxSize: 8 * 1024,
                    },
                },
                generator: {
                    filename: 'fonts/[hash][ext][query]',
                },
            },
            {
                test: /\.css$/,
                use: [
                    MiniCssExtractPlugin.loader,
                    {
                        // eslint-disable-next-line no-undef
                        loader: require.resolve('css-loader'),
                        options: {
                            importLoaders: 1,
                        },
                    },
                    {
                        // eslint-disable-next-line no-undef
                        loader: require.resolve('postcss-loader'),
                    },
                ],
            },
            {
                test: /\.scss$/,
                use: [
                    MiniCssExtractPlugin.loader,
                    {
                        // eslint-disable-next-line no-undef
                        loader: require.resolve('css-loader'),
                        options: {
                            importLoaders: 1,
                        },
                    },
                    {
                        // eslint-disable-next-line no-undef
                        loader: require.resolve('sass-loader'),
                        options: {
                            implementation: sass,
                        },
                    },
                    {
                        // eslint-disable-next-line no-undef
                        loader: require.resolve('postcss-loader'),
                    },
                ],
            },

        ],
    },
    resolve: {
        extensions: ['*', '.js', '.jsx', '.ts', '.tsx'],
        // modules: ['src', 'node_modules']
    },
    output: {
        path: path.resolve(currentDirectory, './public/build'),
        filename: 'js/[name].js',
        publicPath: '/build/',
        assetModuleFilename: 'images/[name][ext]'
    },
    plugins: [
        new CleanWebpackPlugin(),
        new Dotenv({
            path: path.resolve(currentDirectory, './.env'),
        }),
        new webpack.DefinePlugin({
            // eslint-disable-next-line no-undef
            // 'process.env.NODE_ENV': JSON.stringify(process.env.NODE_ENV || 'development'),
            // eslint-disable-next-line no-undef
            // 'process.env.GRAPHQL_URL': JSON.stringify(process.env.GRAPHQL_URL || ''),
        }),
        new MiniCssExtractPlugin({
            filename: 'css/[name].css',
        }),
    ],
};
